package com.sjf.dama;

public class Cell {
	boolean valid;
	Piece token;
	float x;
	float y;
	Cell[] nb;

	boolean visited;
	Cell inDanger;

	public Cell(boolean valid, float x, float y) {
		this.valid = valid;
		this.x = x;
		this.y = y;
		nb = new Cell[4];
	}

	public boolean isEmpty() {
		return token == null;
	}
}
