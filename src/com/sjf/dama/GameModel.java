package com.sjf.dama;

import java.util.LinkedList;
import java.util.List;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

public class GameModel {
	int size;
	Cell[][] cells;
	List<Piece> player1;
	List<Piece> player2;

	List<Piece> takenPlayer1;
	List<Piece> takenPlayer2;

	Cell oldCell;
	Cell currentCell;
	Piece currentToken;

	List<Piece> currentPlayer;
	List<Cell> candidates;

	Rules rules;

	public GameModel(int size, int nb_piece, Paint p1, Paint p2, Rules rules) {
		this.size = size;
		this.rules = rules;

		cells = new Cell[size][size];
		player1 = new LinkedList<Piece>();
		player2 = new LinkedList<Piece>();
		takenPlayer1 = new LinkedList<Piece>();
		takenPlayer2 = new LinkedList<Piece>();
		for (int i = 0; i < nb_piece; i++) {
			player1.add(new Piece(0, p1));
		}
		for (int i = 0; i < nb_piece; i++) {
			player2.add(new Piece(0, p2));
		}

		int p_index = 0;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				boolean valid = false;
				if (rules instanceof ItalianRules) {
					valid = ((i + j) % 2) == 0;
				} else {
					valid = ((i + j) % 2) == 1;
				}
				cells[i][j] = new Cell(valid, 0, 0);
				if (valid) {
					if (i < size / 2 - 1) {
						Piece p = player1.get(p_index++);
						cells[i][j].token = p;

					} else if (i > size / 2) {
						Piece p = player2.get(--p_index);
						cells[i][j].token = p;
					}
				}
			}
		}

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (i > 0 && j > 0) {
					cells[i][j].nb[0] = cells[i - 1][j - 1];
				}
				if (i > 0 && j < size - 1) {
					cells[i][j].nb[1] = cells[i - 1][j + 1];
				}
				if (i < size - 1 && j > 0) {
					cells[i][j].nb[2] = cells[i + 1][j - 1];
				}
				if (i < size - 1 && j < size - 1) {
					cells[i][j].nb[3] = cells[i + 1][j + 1];
				}
			}
		}
		//cells[5][3].token.isDraught = true;
		currentPlayer = player1;
	}

	public void layoutChanged(int width, int height) {
		int cw = width / size;
		int ch = height / size;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (cells[i][j].valid) {
					cells[i][j].x = cw / 2 + cw * j;
					cells[i][j].y = ch / 2 + ch * i;
					if (cells[i][j].token != null) {
						cells[i][j].token.x = cells[i][j].x;
						cells[i][j].token.y = cells[i][j].y;
						cells[i][j].token.size = (float) (Math.min(width,
								height) / size / 2.4);
					}
				}
			}
		}
	}

	public boolean moveToken(Cell c, int j, int i) {
		if (candidates.contains(c)) {
			//transfer the node from one cell to another
			c.token = currentCell.token;
			currentCell.token = null;
			c.token.x = c.x;
			c.token.y = c.y;
			
			if (currentPlayer == player1 && player1.contains(c.token) && i == size - 1
					|| currentPlayer == player2 && player2.contains(c.token) && i == 0) {
				c.token.isDraught = true;
				hasJumpedTo(c);
				changeTurn();
				return true;
			}
			
			// candidates = null;
			if (!hasJumpedTo(c)) {
				changeTurn();
			} else {
				currentCell = c;
				rules.findCandidates(this, j, i, false);
				if (candidates.size() == 0) {
					changeTurn();
				}
			}

			return true;
		}
		return false;
	}

	private void changeTurn() {
		// TODO Auto-generated method stub
		if (currentPlayer == player1) {
			currentPlayer = player2;
		} else {
			currentPlayer = player1;
		}
		candidates = null;
		Log.v("ChangeTurn", " ");
	}

	private boolean hasJumpedTo(Cell target) {
		if (target.inDanger != null && player1.contains(target.inDanger.token)) {
			player1.remove(target.inDanger.token);
			takenPlayer1.add(target.inDanger.token);
			target.inDanger.token = null;
			target.inDanger = null;
			return true;
		} else if (target.inDanger != null && player2.contains(target.inDanger.token)) {
			player2.remove(target.inDanger.token);
			takenPlayer2.add(target.inDanger.token);
			target.inDanger.token = null;
			target.inDanger = null;
			return true;
		}
		return false;
	}

	public void selectCell(Cell c, int j, int i) {
		if (currentPlayer.contains(c.token)) {
			currentCell = c;
			rules.findCandidates(this, j, i, true);
		}
	}

	public void unSelect() {
		currentCell = null;
		candidates = null;
	}
}
