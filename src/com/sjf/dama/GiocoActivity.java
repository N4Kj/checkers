package com.sjf.dama;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class GiocoActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.italian_activity_gioco);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_gioco, menu);
        return true;
    }
}
