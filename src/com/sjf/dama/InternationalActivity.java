package com.sjf.dama;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class InternationalActivity extends Activity implements OnClickListener {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.international_activity_gioco);
	    // TODO Auto-generated method stub
	    Button restart = (Button) findViewById(R.id.restart);
	    restart.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Log.v("Restart Clicked","v ="+v);
		InternationalGameView view = (InternationalGameView) findViewById(R.id.internationalGameView1);
		view.reset();
		
	}

}
