package com.sjf.dama;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class InternationalGameView extends View  {
        private static final int YELLOW = 0;

        GameModel model;
        // Size fixed

        private final int SIZE = 10;
        private final int PIECES = 20;
        private Paint cellPainter;
        private Paint queenPainter;
        public InternationalGameView(Context context, AttributeSet attrs) {
                super(context, attrs);

                init();
        }

        private void init() {
                Paint p1 = new Paint();
                p1.setColor(Color.RED);
                Paint p2 = new Paint();
                p2.setColor(Color.BLUE);
                cellPainter = new Paint();
                cellPainter.setStyle(Paint.Style.STROKE);
                cellPainter.setStrokeWidth(3);
                cellPainter.setColor(Color.YELLOW);
                queenPainter = new Paint();
                queenPainter.setStyle(Paint.Style.STROKE);
                queenPainter.setStrokeWidth(3);
                queenPainter.setColor(Color.YELLOW);
                model = new GameModel(SIZE, PIECES, p1, p2, new InternationalRules());
        }

        public void onLayout(boolean changed, int left, int top, int right,
                        int bottom) {

                model.layoutChanged(getWidth(), getHeight());
        }

        @Override
        public void onDraw(Canvas canvas) {
                for (Piece token : model.player1) {
                        canvas.drawCircle(token.x, token.y, token.size, token.color);// token.size
                        if(token.isDraught) {
                                canvas.drawCircle(token.x, token.y, 5, queenPainter);// token.size
                        }
                }
                for (Piece token : model.player2) {
                        canvas.drawCircle(token.x, token.y, token.size, token.color);// token.size
                        if(token.isDraught) {
                                canvas.drawCircle(token.x, token.y, 5, queenPainter);// token.size
                        }
                }
                float cw = getWidth() / SIZE;
                float ch = getHeight() / SIZE;
                if (model.currentCell != null) {
                        int left = (int) (model.currentCell.x - cw / 2);
                        int top = (int) (model.currentCell.y - ch / 2);
                        int right = (int) (model.currentCell.x + cw / 2);
                        int bottom = (int) (model.currentCell.y + ch / 2);
                        canvas.drawRect(left, top, right, bottom, cellPainter);
                }
                if (model.candidates != null) {

                        for (Cell c : model.candidates) {
                                cellPainter.setARGB(255, 173, 255, 47);
                                int left = (int) (c.x - cw / 2);
                                int top = (int) (c.y - ch / 2);
                                int right = (int) (c.x + cw / 2);
                                int bottom = (int) (c.y + ch / 2);
                                canvas.drawRect(left, top, right, bottom, cellPainter);
                        }

                }

        }

        public InternationalGameView(Context context, AttributeSet attrs, int defStyle) {
                super(context, attrs, defStyle);
        }

        public boolean onTouchEvent(MotionEvent event) {
                float cw = getWidth() / SIZE;
                float ch = getHeight() / SIZE;
                float e_x = event.getX();
                float e_y = event.getY();
                int j = (int) (e_x / cw);
                int i = (int) (e_y / ch);
                Cell c = model.cells[i][j];
                if (event.getAction() == MotionEvent.ACTION_MOVE) { // pnly to read info from cells
                        Log.v("Gameview", "Cell status"+i+" "+j+" "+c.visited );
                }
                if (model.currentCell == null) {
                        if (event.getAction() == MotionEvent.ACTION_UP && !c.isEmpty()) {

                                model.selectCell(c, j, i);
                                invalidate();
                        }
                } else {
                        if (event.getAction() == MotionEvent.ACTION_UP && c.valid
                                        && !c.isEmpty()) {
                                model.currentCell = null;
                        } else if (event.getAction() == MotionEvent.ACTION_UP && c.valid
                                        && c.isEmpty()) {
                                Log.v("GAmeView cell clicked at ", j + " " + i);
                                model.moveToken(c, j, i);
                                model.currentCell = null;
                        }
                        invalidate();
                }
                return true;
        }

		public void reset() {
			Paint p1 = new Paint();
            p1.setColor(Color.RED);
            Paint p2 = new Paint();
            p2.setColor(Color.BLUE);
			model = new GameModel(SIZE, PIECES, p1, p2, new InternationalRules());
			model.layoutChanged(getWidth(), getHeight());
			invalidate();
		}
}