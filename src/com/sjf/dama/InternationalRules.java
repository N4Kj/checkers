package com.sjf.dama;

import java.util.LinkedList;

import android.util.Log;

public class InternationalRules implements Rules {
	private void findInDiagonal(GameModel model, int x, int y, int dx, int dy, int d) {
		// TODO Reverse order for d, make it start from 1 to dmax.
		//It will enable to stop the research of a new candidates if we accounter a token
		if( d == 0 ) {
			return;
		} else if ( d > 1) {
			findInDiagonal(model, x, y, dx, dy, d-1);
		}
		int nx = x + dx*d;
		int ny = y + dy*d;
		if (nx >= 0 && nx < model.size && ny >= 0 && ny < model.size) {
			
			Cell c = model.cells[nx][ny];
			if (c.isEmpty()) {
				model.candidates.add(c);
			} else {
				if (model.currentPlayer == model.player1 && model.player2.contains(c.token)) {
					findInDiagonal(model, nx, ny, dx, dy, 1);
				}
				else if (model.currentPlayer == model.player2 && model.player1.contains(c.token)) {
					findInDiagonal(model, nx, ny, dx, dy, 1);
				}
			}
		}
	}
	public void findCandidates(GameModel model, int j, int i,boolean allowSimpleMove) {
		model.candidates = new LinkedList<Cell>();
		if (model.currentPlayer == model.player1) { // goes down
			if (model.currentCell.token.isDraught) {
				findInDiagonal(model, i, j, 1, -1, model.size-1);
				findInDiagonal(model, i, j, 1, 1, model.size-1);
			} else {
				findInDiagonal(model, i, j, 1, -1, 1);
				findInDiagonal(model, i, j, 1, 1, 1);
			}
			
		} else { // goes up
			if (model.currentPlayer == model.player2) { // goes down
				if (model.currentCell.token.isDraught) {
					findInDiagonal(model, i, j, -1, 1, model.size-1);
					findInDiagonal(model, i, j, -1, -1, model.size-1);
				} else {
					findInDiagonal(model, i, j, -1, 1, 1);
					findInDiagonal(model, i, j, -1, -1, 1);
				}
			}
		}
	}
}
