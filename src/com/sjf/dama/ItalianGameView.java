package com.sjf.dama;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class ItalianGameView extends View {
	GameModel model;
	// Size fixed

	private final int SIZE = 8;
	private final int PIECES = 12;

	private Paint cellPainter;
	private Paint queenPainter;

	public ItalianGameView(Context context) {
		super(context);
		init();

	}

	public ItalianGameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();

	}

	public ItalianGameView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		Paint p1 = new Paint();
		p1.setColor(Color.RED);
		Paint p2 = new Paint();
		p2.setColor(Color.BLUE);
		cellPainter = new Paint();
		cellPainter.setColor(Color.GREEN);
		cellPainter.setStyle(Paint.Style.STROKE);
		cellPainter.setStrokeWidth(5);
		queenPainter = new Paint();
		queenPainter.setColor(Color.YELLOW);
		queenPainter.setStyle(Paint.Style.STROKE);
		queenPainter.setStrokeWidth(5);
		model = new GameModel(SIZE, PIECES, p1, p2, new ItalianRules());
	}

	@Override
	public void onLayout(boolean change, int left, int top, int right,
			int bottom) {
		Log.v("Gameview", "Size = " + getWidth());
		model.layoutChanged(getWidth(), getHeight());
	}

	@Override
	public void onDraw(Canvas canvas) {
		float cw = getWidth() / SIZE;
		float ch = getHeight() / SIZE;
		for (Piece token : model.player1) {
			canvas.drawCircle(token.x, token.y, token.size, token.color);
			if (token.isDraught) {
				canvas.drawCircle(token.x, token.y, 5, queenPainter);// token.size
			}
		}
		for (Piece token : model.player2) {
			canvas.drawCircle(token.x, token.y, token.size, token.color);
			if (token.isDraught) {
				canvas.drawCircle(token.x, token.y, 5, queenPainter);// token.size
			}
		}
		if (model.currentCell != null) {
			int left = (int) (model.currentCell.x - cw / 2);
			int top = (int) (model.currentCell.y - ch / 2);
			int right = (int) (model.currentCell.x + cw / 2);
			int bottom = (int) (model.currentCell.y + ch / 2);
			cellPainter.setColor(Color.GREEN);
			canvas.drawRect(left, top, right, bottom, cellPainter);
		}
		if (model.candidates != null) {
			for (Cell c : model.candidates) {
				cellPainter.setARGB(255, 173, 255, 47);
				int left = (int) (c.x - cw / 2);
				int top = (int) (c.y - ch / 2);
				int right = (int) (c.x + cw / 2);
				int bottom = (int) (c.y + ch / 2);
				canvas.drawRect(left, top, right, bottom, cellPainter);
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float cw = getWidth() / SIZE;
		float ch = getHeight() / SIZE;
		float e_x = event.getX();
		float e_y = event.getY();
		int j = (int) (e_x / cw);
		int i = (int) (e_y / ch);
		if (i < 0 || i >= model.size || j < 0 || j >= model.size)
			return true;
		Cell c = model.cells[i][j];
		if (model.currentCell == null) {
			if (event.getAction() == MotionEvent.ACTION_UP && c.valid
					&& !c.isEmpty()) {
				model.selectCell(c, j, i);
				invalidate();
			}
		} else {
			if (event.getAction() == MotionEvent.ACTION_UP && c.valid
					&& !c.isEmpty()) {
				model.unSelect();

			} else if (event.getAction() == MotionEvent.ACTION_UP && c.valid
					&& c.isEmpty()) {
				model.moveToken(c, j, i);
				model.currentCell = null;
			}
			invalidate();
		}
		return true;
	}
}
