package com.sjf.dama;

import java.util.Currency;
import java.util.LinkedList;
import java.util.List;

import android.util.Log;

public class ItalianRules implements Rules {

	public void findCandidates(GameModel model, int j, int i, boolean allowSimpleMove) {
		model.candidates = new LinkedList<Cell>();
		findPaths(model, j, i, allowSimpleMove);
	}

	private void unVisit(GameModel model) {
		for (int i = 0; i < model.size; i++) {
			for (int j = 0; j < model.size; j++) {
				if (model.cells[i][j] != null)
					model.cells[i][j].visited = false;
			}
		}
	}
	
	private void pathForQueen(GameModel model, boolean allowSimpleMove ) {
		Tuple[] results = new Tuple[4];
		results[0] = computePath(model.currentCell, model,
				model.currentCell.nb[0], 0, 0);
		unVisit(model);
		results[1] = computePath(model.currentCell, model,
				model.currentCell.nb[1], 1, 0);
		unVisit(model);
		results[2] = computePath(model.currentCell, model,
				model.currentCell.nb[2], 2, 0);
		unVisit(model);
		results[3] = computePath(model.currentCell, model,
				model.currentCell.nb[3], 3, 0);
		unVisit(model);
		Tuple best = null;
		int max;
		if (allowSimpleMove) {
			max = 0;
		} else {
			max = 1;
		}
		for (int k = 0; k < results.length; k++) {
			if (results[k].max > max) {
				model.candidates.clear();
				best = results[k];
				max = results[k].max;
				addFilterNull(model.candidates, best);
			} else if (results[k].max == max) {
				addFilterNull(model.candidates, results[k]);
			}
		}
	}
	
	private void pathForToken(GameModel model, boolean allowSimpleMove, int d1, int d2) {
		Tuple[] results = new Tuple[4];
		results[d1] = computePath(model.currentCell, model,
				model.currentCell.nb[d1], d1, 0);
		unVisit(model);
		results[d2] = computePath(model.currentCell, model,
				model.currentCell.nb[d2], d2, 0);
		unVisit(model);
		// Do not Add the result if it was 0
		int max;
		if (allowSimpleMove) {
			max = 0;
		} else {
			max = 1;
		}
		if (results[d1].max == results[d2].max && results[d1].max > max && results[d2].max> max) {
			addFilterNull(model.candidates, results[d1]);
			addFilterNull(model.candidates, results[d2]);
		} else if (results[d1].max < results[d2].max && results[d2].max > max) {
			addFilterNull(model.candidates, results[d2]);
		} else if (results[d1].max > max){
			addFilterNull(model.candidates, results[d1]);
		}
	}
	
	public void findPaths(GameModel model, int j, int i, boolean allowSimpleMove) {
		model.candidates = new LinkedList<Cell>();
		if (model.currentPlayer == model.player1) {
			if (model.currentCell.token.isDraught) {
				pathForQueen(model, allowSimpleMove);
			} else {
				pathForToken(model, allowSimpleMove, 2,3);
			}
		} else {
			if (model.currentCell.token.isDraught) {
				pathForQueen(model, allowSimpleMove);
			} else {
				// use nb1 and nb3
				pathForToken(model, allowSimpleMove, 0,1);
//				
			}
		}
	}

	private Tuple computePath(Cell currentCell, GameModel model, Cell c,
			int direction, int depth) {
		int max = 0;
		Tuple t = new Tuple();
		t.max = depth + 1;
		t.c = c;
		// Log.v("computapath","curr"+currentCell+" c:"+c+" "+c.visited);
		if (c == null || currentCell.visited || c.visited
				|| model.currentPlayer.contains(c.token)) {
			// Log.v("computapath","curr verified"+currentCell.visited+" c:"+c+" "+c.visited);
			currentCell.visited = true; // in case we entered here for case 2
										// and 3
			t.max = depth;
			t.c = null;
		} else {
			currentCell.visited = true;
			if (!c.isEmpty()) {
				Cell next = c.nb[direction];
				Log.v("ComputePath", "inspecting jump in direction "
						+ direction);
				if (next != null && next.isEmpty()) {
				//if (next != null && next.isEmpty() && !c.token.isDraught) {
					Log.v("ComputePath", "Jump in dir " + direction);
					// jump to it and compute recursively in all 4 directions
					t.c = next;
					//model.inDanger = c;
					if (next.nb[0] != null)
						max = Math.max(
								max,
								computePath(next, model, next.nb[0], 0,
										depth + 1).max);
					if (next.nb[1] != null)
						max = Math.max(
								max,
								computePath(next, model, next.nb[1], 1,
										depth + 1).max);
					if (next.nb[2] != null)
						max = Math.max(
								max,
								computePath(next, model, next.nb[2], 2,
										depth + 1).max);
					if (next.nb[3] != null)
						max = Math.max(
								max,
								computePath(next, model, next.nb[3], 3,
										depth + 1).max);
					Log.v("CompuPath", "return " + max);
					
					t.inDanger = c;
					t.max = max;
				}
			}
		}
		return t;
	}

	private void addFilterNull(List<Cell> l, Tuple t) {
		Log.v("Add Filter","List :"+l+" Tuple "+t);
		if (t.c != null && t.c.isEmpty()) {
			l.add(t.c);
			t.c.inDanger = t.inDanger;
		}
	}
}

class Tuple {
	int max;
	Cell c;
	Cell inDanger;
}
