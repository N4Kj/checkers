package com.sjf.dama;

import android.graphics.Paint;

public class Piece {
	float size;
	float x;
	float y;
	Paint color;

	boolean isDraught;

	public Piece(float size, Paint color) {
		this.size = size;
		this.color = color;
	}
}
