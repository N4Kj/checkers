package com.sjf.dama;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class WelcomeActivity extends Activity implements OnClickListener {
	 Button italianButton;
	 Button international;
	 Button freeButton;
	 Button creditsButton;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.welcome_activity);
	    italianButton = (Button) findViewById(R.id.button2);
	    international = (Button) findViewById(R.id.button1);
	    freeButton = (Button) findViewById(R.id.button4);
	    creditsButton = (Button) findViewById(R.id.button3);
	    italianButton.setOnClickListener(this);
	    international.setOnClickListener(this);
	    freeButton.setOnClickListener(this);
	    creditsButton.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		Intent intent;
		if (v == italianButton) {
			intent = new Intent(this, GiocoActivity.class);
			this.startActivity(intent);
		} else if ( v == international) {
			intent = new Intent(this, InternationalActivity.class);
			this.startActivity(intent);
		} else if ( v == creditsButton) {
			intent = new Intent(this, CreditsActivity.class);
			this.startActivity(intent);
		}
	}
	
}
